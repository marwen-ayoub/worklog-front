import { ref } from 'vue';
import AuthService from '@/service/AuthService';
import type { UserRoleType } from '@/types/userTypes';

const collaboratorId = ref<string | undefined>();
const managerId = ref<string | undefined>();
const userRole = ref<UserRoleType>();

export function useGetUserRole() {
    AuthService.getClaims().then((claims) => {
        if (claims && claims.realm_access?.roles) {
            if (claims.realm_access.roles.includes('COLLABORATOR')) {
                if (claims.sub && claims.manager_id) {
                    collaboratorId.value = claims.sub;
                    managerId.value = claims.manager_id;
                    userRole.value = 'COLLABORATOR';
                }
            } else if (claims.realm_access.roles.includes('MANAGER')) {
                if (claims.sub) {
                    managerId.value = claims.sub;
                    userRole.value = 'MANAGER';
                }
            }
        }
    });

    return {
        collaboratorId,
        managerId,
        userRole
    };
}
