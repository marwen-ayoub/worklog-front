import axios from '@/utils/axios';
import { createWebHistory, createRouter, type NavigationGuardNext, type RouteLocation } from 'vue-router';

import DashboardLayout from '@/layout/AppLayout.vue';
import AuthService from '@/service/AuthService';
import { useGetUserRole } from '@/composables/useGetUserRole';

const { userRole } = useGetUserRole();

function collaboratorGuard(_: RouteLocation, _1: RouteLocation, next: NavigationGuardNext) {
    if (userRole.value === 'COLLABORATOR') {
        next();
    } else {
        next({ name: 'unauthorized' });
    }
}

function managerGuard(_: RouteLocation, _1: RouteLocation, next: NavigationGuardNext) {
    if (userRole.value === 'MANAGER') {
        next();
    } else {
        next({ name: 'unauthorized' });
    }
}

function sharedGuard(to: RouteLocation, _: RouteLocation, next: NavigationGuardNext) {
    if (to.name === 'addTimesheet') {
        if (userRole.value === 'MANAGER') {
            if (to.query.month && to.query.year && to.query.collaboratorId && to.query.monthlyTimesheetId) {
                next();
            } else {
                next({ name: 'errorPage' });
            }
        } else if (userRole.value === 'COLLABORATOR') {
            next();
        }
    } else if (to.name === 'dashboard') {
        if (userRole.value === 'COLLABORATOR') {
            next();
        } else if (userRole.value === 'MANAGER') {
            next();
        }
    } else {
        next('unauthorized');
    }
}

const router = createRouter({
    history: createWebHistory(),
    routes: [
        {
            path: '/missed-profile-info',
            name: 'missed-profile-info',
            component: () => import('@/views/collaborators/IncompletedInformationPage.vue'),
            beforeEnter: [collaboratorGuard]
        },
        {
            path: '/callback',
            name: 'auth_callback',
            component: () => import('@/views/auth/SigninCallback.vue')
        },
        {
            path: '/unauthorized',
            name: 'unauthorized',
            component: () => import('@/views/errorsPages/UnauthorizedPage.vue')
        },
        {
            path: '/error',
            name: 'errorPage',
            component: () => import('@/views/errorsPages/ErrorPage.vue')
        },
        {
            path: '/',
            component: DashboardLayout,
            children: [
                {
                    path: '/',
                    name: 'dashboard',
                    component: () => import('@/views/collaborators/DashboardPage.vue'),
                    beforeEnter: [sharedGuard]
                },
                {
                    path: '/leave-request/add',
                    name: 'addLeaveRequest',
                    component: () => import('@/views/collaborators/AddLeaveRequestPage.vue'),
                    beforeEnter: [collaboratorGuard]
                },
                {
                    path: '/leave-request',
                    name: 'myLeaveRequests',
                    component: () => import('@/views/collaborators/MyLeaveRequestPage.vue'),
                    beforeEnter: [collaboratorGuard]
                },
                {
                    path: '/timesheet/add',
                    name: 'addTimesheet',
                    props: true,
                    component: () => import('@/views/collaborators/AddTimesheetPage.vue'),
                    beforeEnter: [sharedGuard]
                },
                {
                    path: '/timesheet/imputations',
                    name: 'myImputationRequests',
                    component: () => import('@/views/collaborators/MyImputationRequestsPage.vue'),
                    beforeEnter: [collaboratorGuard]
                },
                {
                    path: '/country',
                    name: 'country',
                    component: () => import('@/views/manager/ManageCountriesAndPublicDaysPage.vue'),
                    beforeEnter: [managerGuard]
                },
                {
                    path: '/projects',
                    name: 'projects',
                    component: () => import('@/views/manager/ManageProjectPage.vue'),
                    beforeEnter: [managerGuard]
                },
                {
                    path: '/assign-project',
                    name: 'assignProjects',
                    component: () => import('@/views/manager/AssignProjectPage.vue'),
                    beforeEnter: [managerGuard]
                },
                {
                    path: '/imputation-requests',
                    name: 'ImputationList',
                    component: () => import('@/views/manager/ManageImputationRequestsPage.vue'),
                    beforeEnter: [managerGuard]
                },
                {
                    path: '/vacation-types',
                    name: 'vacationTypes',
                    component: () => import('@/views/manager/ManageVacationTypesPage.vue'),
                    beforeEnter: [managerGuard]
                },
                {
                    path: '/manager/vacation-requests',
                    name: 'managerVacationRequestList',
                    component: () => import('@/views/manager/ManagerVacationRequestListPage.vue'),
                    beforeEnter: [managerGuard]
                },
                {
                    path: '/vacation-request/:id/confirmation',
                    name: 'confirmVacationRequestPage',
                    component: () => import('@/views/manager/ConfirmVacationRequestPage.vue'),
                    props: true,
                    beforeEnter: [managerGuard]
                }
            ]
        },
        {
            path: '/:pathMatch(.*)',
            name: 'not-found',
            component: () => import('@/views/errorsPages/ErrorPage.vue')
        }
    ],
    strict: true
});

router.beforeEach((to, _, next) => {
    console.log('BEFORE EACH');
    if (to.name === 'auth_callback') next();
    else
        AuthService.getUser().then((user) => {
            if (user) {
                console.log('USER IS LOGGED IN');
                axios.defaults.headers.common['Authorization'] = `Bearer ${user.access_token}`;
                next();
            } else {
                console.log('USER IS NOT LOGGED IN');
                AuthService.login();
            }
        });
});

export default router;
