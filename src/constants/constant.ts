export const ImputationStatusList = ['APPROVED', 'PENDING', 'REJECTED'] as const;

export const LeaveStatusType = ['PENDING', 'APPROVED', 'REJECTED'] as const;

export const TimesheetInputRowType = ['mondayEntry', 'tuesdayEntry', 'wednesdayEntry', 'thursdayEntry', 'fridayEntry'] as const;

export const ImputationMonthlyStatusList = ['APPROVED', 'PENDING', 'REJECTED'] as const;

export const SaveImputationType = ['DRAFT', 'APPROVAL'] as const;

export const Role = ['Collaborator', 'Manager'] as const;

export const ExportArrayType = ['EXCEL', 'PDF'] as const;

export enum TIMESHEET_COLUMN_DAY {
    MONDAY = 'mondayEntry',
    TUESDAY = 'tuesdayEntry',
    WEDNESDAY = 'wednesdayEntry',
    THURSDAY = 'thursdayEntry',
    FRIDAY = 'fridayEntry'
}

export enum IMPUTATION_STATUS {
    APPROVED = 'APPROVED',
    REJECTED = 'REJECTED',
    PENDING = 'PENDING'
}
export enum LEAVE_STATUS {
    APPROVED = 'APPROVED',
    REJECTED = 'REJECTED',
    PENDING = 'PENDING'
}
export enum IMPUTATION_MONTHLY_STATUS {
    APPROVED = 'APPROVED',
    REJECTED = 'REJECTED',
    PENDING = 'PENDING'
}
export enum ROLE {
    COLLABORATOR = 'COLLABORATOR',
    MANAGER = 'MANAGER'
}

export enum MANAGER_RESPONSE {
    APPROVED = 'APPROVED',
    REJECTED = 'REJECTED'
}

export enum IMPUTATION_TYPE {
    DRAFT = 'DRAFT',
    APPROVAL = 'APPROVAL'
}

export enum VACATION_TIME {
    MORNING = 9,
    NOON = 13,
    EVENING = 19
}

export enum VACATION_DAY_HOURS {
    HALF = 4,
    FULL = 8
}

export enum EXPORT_TYPE {
    PDF = 'PDF',
    EXCEL = 'EXCEL'
}
