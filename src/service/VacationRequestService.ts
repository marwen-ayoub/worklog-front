import type { NewVacationRequest, Status } from '@/types/vacationRequestType';
import axios from '@/utils/axios';

function addVacationRequest(collaboratorId: string, managerId: string, vacationRequestObject: NewVacationRequest) {
    return axios.post(`/api/v1/collaborator/${collaboratorId}/vacation-request`, vacationRequestObject, { params: { manager_id: managerId } });
}

function getVacationRequestListPaginated(collaboratorId: string, page: number, size: number, upcomming: boolean = false) {
    return axios.get(`/api/v1/collaborator/${collaboratorId}/vacation-request`, { params: { page, size, upcomming } });
}

function getManagerVacationRequestPaginated(managerId: string, page: number, size: number, latest: boolean = false) {
    return axios.get(`/api/v1/manager/${managerId}/vacation-request`, { params: { page, size, latest } });
}

function deleteVacationRequestById(collaboratorId: string, vacationRequestId: string) {
    return axios.delete(`/api/v1/collaborator/${collaboratorId}/vacation-request/${vacationRequestId}`);
}
function getVacationRequestById(collaboratorId: string, vacationRequestId: string) {
    return axios.get(`/api/v1/collaborator/${collaboratorId}/vacation-request/${vacationRequestId}`);
}

function updateVacationRequestStatus(managerId: string, vacationRequestId: string, status: Status) {
    return axios.put(`/api/v1/manager/${managerId}/vacation-request/${vacationRequestId}`, {}, { params: { Status: status } });
}
function getApprovedVacationDatesListByType(collaboratorId: string, vacationTypeId: string, Status: Status, startDate: string, endDate: string) {
    return axios.get(`/api/v1/collaborator/${collaboratorId}/vacation-requests/vacation-types/${vacationTypeId}`, { params: { Status, startDate, endDate } });
}
function getApprovedVacationDatesList(collaboratorId: string, Status: Status, startDate: string, endDate: string) {
    return axios.get(`/api/v1/collaborator/${collaboratorId}/vacation-requests/dates`, { params: { Status, startDate, endDate } });
}

export default {
    addVacationRequest,
    updateVacationRequestStatus,
    getVacationRequestListPaginated,
    getManagerVacationRequestPaginated,
    deleteVacationRequestById,
    getVacationRequestById,
    getApprovedVacationDatesListByType,
    getApprovedVacationDatesList
};
