const contextPath = import.meta.env.BASE_URL;

function getNextVacationSmall() {
    return fetch(contextPath + 'demo/data/next-leave-items.json')
        .then((res) => res.json())
        .then((d) => d.data);
}

function getAllVacationTypeByEntity() {
    return fetch(contextPath + 'demo/data/allVacationTypeByEntity.json')
        .then((res) => res.json())
        .then((d) => d.data);
}

function addVacationRequest(VacationData: any) {
    return fetch('api/vacationRequest').then(() => ({
        id: 32,
        vacationTypeDto: VacationData?.vacationTypeDto,
        userId: VacationData?.userId
    }));
}

function getAllMyVacationRequest() {
    return fetch(contextPath + 'demo/data/MyVacationRequests.json')
        .then((res) => res.json())
        .then((d) => d.data);
}

function getAllLeaveBalance() {
    return fetch(contextPath + 'demo/data/LeaveBalance.json')
        .then((res) => res.json())
        .then((d) => d.data);
}

export default {
    getNextVacationSmall,
    getAllVacationTypeByEntity,
    addVacationRequest,
    getAllMyVacationRequest,
    getAllLeaveBalance
};
