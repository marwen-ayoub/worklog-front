import axios from '@/utils/axios';

import type { UserType } from '@/types/userTypes';

function synchronizeCollaborator(Collaborator: UserType) {
    console.log(Collaborator);
    return axios.post(`/api/v1/collaborator`, Collaborator);
}
function getCollaboratorById(collaboratorId: string) {
    return axios.get(`/api/v1/collaborator/${collaboratorId}`);
}
function getMyCollaboratorList(managerId: string) {
    return axios.get(`/api/v1/manager/${managerId}/collaborators`);
}

export default {
    synchronizeCollaborator,
    getCollaboratorById,
    getMyCollaboratorList
};
