import type { ProjectType } from '@/types/projectType';
import axios from '@/utils/axios';

function getAllProjects() {
    return axios.get('/api/v1/project');
}

function editProjectById(projectID: string, projectName: string) {
    return axios.put(`/api/v1/project/${projectID}`, { projectName });
}
function addProjectById(projectName: ProjectType) {
    return axios.post('/api/v1/project', projectName);
}
function deleteProjectById(projectId: string) {
    return axios.delete(`/api/v1/project/${projectId}`);
}

export default {
    getAllProjects,
    editProjectById,
    addProjectById,
    deleteProjectById
};
