import type { vacationType } from '@/types/vacationType';
import axios from '@/utils/axios';

function getAllVacationTypes() {
    return axios.get('/api/v1/vacation-type');
}

function addVacationType(vacationTypeObject: vacationType) {
    return axios.post('/api/v1/vacation-type', vacationTypeObject);
}
function deleteVacationTypeById(vacationTypeId: string) {
    return axios.delete(`/api/v1/vacation-type/${vacationTypeId}`);
}

function updateVacationTypeById(vacationTypeId: string, updatedVacationType: vacationType) {
    return axios.put(`/api/v1/vacation-type/${vacationTypeId}`, updatedVacationType);
}

export default {
    getAllVacationTypes,
    addVacationType,
    deleteVacationTypeById,
    updateVacationTypeById
};
