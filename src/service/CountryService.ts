import type { newPublicDayType } from '@/types/countryType';
import axios from '@/utils/axios';
import type { components } from './schema';

type CountryType = components['schemas']['Country'];

function searchCountryByName(countryName: CountryType) {
    return axios.get(`/api/v1/country/name`, { params: { name: countryName } });
}

function getAllCountries() {
    return axios.get('/api/v1/country');
}

function deleteCountryById(countryId: string) {
    return axios.delete(`/api/v1/country/${countryId}`);
}
function editCountryById(countryId: string, countryName: string) {
    return axios.put(`/api/v1/country/${countryId}`, { countryName });
}
function addCountry(countryName: string) {
    return axios.post('/api/v1/country', { countryName });
}

function addPublicDaysToCountry(countryId: string, publicDayList: newPublicDayType[]) {
    return axios.put(`/api/v1/country/${countryId}/public-days`, publicDayList);
}

export default {
    searchCountryByName,
    getAllCountries,
    editCountryById,
    deleteCountryById,
    addCountry,
    addPublicDaysToCountry
};
