import axios from '@/utils/axios';
import type { ImputationTimesheetDraft } from '@/types/imputationTypes';
import type { ExportType } from '@/types/exportType';

const contextPath = import.meta.env.BASE_URL;

function getImputationSmall() {
    return fetch(contextPath + 'demo/data/imputation-small.json')
        .then((res) => res.json())
        .then((d) => d.data);
}
function getWeeklyTimesheet(collaboratorId: string, startDate: string, endDate: string) {
    return axios.get(`/api/v1/collaborator/${collaboratorId}/timesheet/${startDate}/${endDate}`);
}
function getMonthlyTimesheetStatus(collaboratorId: string, managerId: string, month: number, year: number) {
    return axios.get(`/api/v1/monthly-timesheet/manager/${managerId}/collaborator/${collaboratorId}/${month}/${year}`);
}

function saveWeeklyDraft(collaboratorId: string, manager_id: string, startWeekDate: string, endWeekDate: string, imputationDraft: ImputationTimesheetDraft[]) {
    return axios.post(`/api/v1/collaborator/${collaboratorId}/timesheet`, imputationDraft, { params: { startWeekDate, endWeekDate, manager_id } });
}

function getPublicHoliday(collaboratorId: string) {
    return axios.get(`/api/v1/collaborator/${collaboratorId}/public-days`);
}

function saveMonthlyImputation(collaboratorId: string, managerId: string, timesheetMonth: number, timesheetYear: number) {
    return axios.post(`/api/v1/monthly-timesheet/manager/${managerId}/collaborator/${collaboratorId}`, { timesheetMonth, timesheetYear });
}

function getOffProject(collaboratorId: string, startWeekDate: string, endWeekDate: string) {
    return axios.get(`/api/v1/collaborator/${collaboratorId}/timesheet/off-projects/${startWeekDate}/${endWeekDate}`);
}
function getLatestImputationRequests(collaboratorId: string, managerId: string, latest: boolean = true, page: number = 0, size: number = 3) {
    return axios.get(`/api/v1/monthly-timesheet/collaborator/${collaboratorId}`, { params: { latest, managerId, page, size } });
}
function getImputationRequestByManagerId(managerId: string, latest: boolean) {
    return axios.get(`/api/v1/monthly-timesheet/manager/${managerId}`, { params: { latest } });
}

function sendManagerResponse(managerResponse: 'APPROVED' | 'REJECTED', monthlyTimesheetId: string) {
    return axios.put(`/api/v1/monthly-timesheet/${monthlyTimesheetId}`, { monthStatus: managerResponse });
}

function getMonthlyTimesheetRequestCountByCollaboratorId(collaboratorId: string) {
    return axios.get(`/api/v1/monthly-timesheet/collaborator/${collaboratorId}/count`);
}

function getMonthlyTimesheetRequestCountByManagerId(managerId: string) {
    return axios.get(`/api/v1/monthly-timesheet/manager/${managerId}/count`);
}
function getApprovedVacationRequests(collaboratorId: string, startDate: string, endDate: string, status: 'APPROVED') {
    return axios.get(`/api/v1/collaborator/${collaboratorId}/vacation-requests/dates`, { params: { startDate, endDate, Status: status } });
}

function exportTimesheet(collaboratorId: string, month: number, year: number, exportType: ExportType) {
    return axios.get(`/api/v1/collaborator/${collaboratorId}/monthly-timesheet/${month}/${year}/export`, {
        params: { 'export-type': exportType },
        responseType: 'arraybuffer'
    });
}

export default {
    getMonthlyTimesheetRequestCountByCollaboratorId,
    getMonthlyTimesheetRequestCountByManagerId,
    getImputationSmall,
    getWeeklyTimesheet,
    getMonthlyTimesheetStatus,
    saveWeeklyDraft,
    getPublicHoliday,
    saveMonthlyImputation,
    getOffProject,
    getLatestImputationRequests,
    getImputationRequestByManagerId,
    sendManagerResponse,
    getApprovedVacationRequests,
    exportTimesheet
};
