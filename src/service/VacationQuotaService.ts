import axios from '@/utils/axios';

function getVacationDaysQuotaByCollaboratorAndVacationType(collaboratorId: string, vacationTypeId: string) {
    return axios.get(`/api/v1/collaborator/${collaboratorId}/vacation-type/${vacationTypeId}/vacation_quota`);
}

function getVacationQuotaByCollaboratorId(collaboratorId: string) {
    return axios.get(`/api/v1/collaborator/${collaboratorId}/vacation-days-quota`);
}

export default {
    getVacationDaysQuotaByCollaboratorAndVacationType,
    getVacationQuotaByCollaboratorId
};
