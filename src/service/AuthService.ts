import type { ExtendedIdTokenClaims } from '@/types/ExtendedIdTokenClaims';
import { User, UserManager, WebStorageStateStore } from 'oidc-client-ts';
import axios from '@/utils/axios';

const userManager: UserManager = new UserManager({
    userStore: new WebStorageStateStore({ store: window.localStorage }),
    authority: 'http://localhost:8090/realms/GoPartner',
    client_id: 'Worklog',
    redirect_uri: window.location.origin + '/callback',
    response_type: 'code',
    scope: 'openid address',
    post_logout_redirect_uri: window.location.origin + '/',
    revokeTokensOnSignout: true,
    includeIdTokenInSilentSignout: true,
    automaticSilentRenew: true,
    filterProtocolClaims: true,
    loadUserInfo: false
});

userManager.events.addUserLoaded((newUser) => {
    console.log('USER LOADED EVENT');
    userManager.storeUser(newUser);
    axios.defaults.headers.common['Authorization'] = `Bearer ${newUser.access_token}`;
});

userManager.events.addAccessTokenExpired(() => {
    console.log('Access Token Expired  EVENT');
    userManager.signoutRedirect();
});

userManager.events.addSilentRenewError((error) => {
    console.log('ERROR RENEWING ACCESS TOKEN.');
    console.log(error);
});

console.log('USER MANAGER CREATED');
export default class AuthService {
    public static getUser(): Promise<User | null> {
        return userManager.getUser();
    }

    public static async login(): Promise<void> {
        return userManager.signinRedirect();
    }
    public static async signinCallback(): Promise<void | User> {
        return userManager.signinCallback();
    }

    public static async logout(): Promise<void> {
        const user = await userManager.getUser();
        localStorage.clear();
        return userManager.signoutRedirect({ id_token_hint: user?.id_token });
    }

    //get claims from id token
    public static async getClaims(): Promise<ExtendedIdTokenClaims | null> {
        const user = await userManager.getUser();
        return (user?.profile as ExtendedIdTokenClaims | null) ?? null;
    }

    // Decode the access token and get its claims
    public static async getAccessTokenRoleClaim(): Promise<any | null> {
        const user = await userManager.getUser();
        const token = user?.access_token;

        if (token) {
            // Decode the access token to get its claims
            const base64Url = token.split('.')[1];
            const base64 = base64Url.replace(/-/g, '+').replace(/_/g, '/');
            const jsonPayload = decodeURIComponent(
                atob(base64)
                    .split('')
                    .map((c) => '%' + ('00' + c.charCodeAt(0).toString(16)).slice(-2))
                    .join('')
            );

            const claims = JSON.parse(jsonPayload);
            return claims?.realm_access?.roles;
        }

        return null;
    }
}
