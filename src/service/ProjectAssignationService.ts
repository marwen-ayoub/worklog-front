import type { AssignProjectType } from '@/types/assignProjectType';
import axios from '@/utils/axios';

function addProjectAssignation(managerId: string, assignedProjectObject: AssignProjectType) {
    return axios.post(`/api/v1/manager/${managerId}/assigned-project`, assignedProjectObject);
}
function getAllAssignedProject(managerId: string) {
    return axios.get(`/api/v1/manager/${managerId}/assigned-project`);
}
function deleteAssignedProjectById(assignedPRojectId: string, projectStartDate: string, projectEndDate: string) {
    return axios.delete(`/api/v1/assigned-project/${assignedPRojectId}`, { params: { project_start_date: projectStartDate, project_end_date: projectEndDate } });
}

export default {
    addProjectAssignation,
    getAllAssignedProject,
    deleteAssignedProjectById
};
