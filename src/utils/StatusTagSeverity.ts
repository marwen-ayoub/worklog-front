import type { ImputationStatus, Status } from '@/types/types';
import { i18n } from '@/config/i18n';

const { t } = i18n.global;

export const getSeverity = (status: Status) => {
    switch (status) {
        case 'PENDING':
            return 'warning';
        case 'APPROVED':
            return 'success';
        case 'REJECTED':
            return 'danger';
        default:
            return 'info';
    }
};
export const getImputationStatusSeverity = (status: ImputationStatus) => {
    switch (status) {
        case 'APPROVED':
            return 'success';
        case 'PENDING':
            return 'warning';
        case 'REJECTED':
            return 'danger';
    }
};
export const handleVacationSeverity = (index: number) => {
    const severity = ['success', 'danger', 'info', 'warning', 'secondary', 'help'];
    return severity[index];
};

export const handleNexTLeaveStatusText = (status: Status) => {
    switch (status) {
        case 'APPROVED':
            return t('vacationStatus.APPROVED');
        case 'PENDING':
            return t('vacationStatus.PENDING');
        case 'REJECTED':
            return t('vacationStatus.REJECTED');
    }
};
