import { i18n } from '@/config/i18n';
const { t } = i18n.global;
export const getToastMessage = (message: string) => t(message);
