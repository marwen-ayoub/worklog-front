import axios from 'axios';

export default axios.create({
    timeout: 20000,
    baseURL: 'http://localhost:8080'
});
