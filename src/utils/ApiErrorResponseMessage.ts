import { i18n } from '@/config/i18n';
const { t } = i18n.global;
export const handleApiErrorMessage = (errorCode: number) => {
    if (errorCode) return t(`apiErrorMessage[${errorCode}]`);
    else return t('errorFetchingMessage');
};
