import type { Meta, StoryObj } from '@storybook/vue3';
import { userEvent, within } from '@storybook/testing-library';
import { sleep } from '@/utils/SleepFunction';

import { expect } from '@storybook/jest';
import LeaveRequestForm from '@/components/LeaveRequestForm.vue';
import moment from 'moment';

const meta: Meta<typeof LeaveRequestForm> = {
    title: 'Leave Request Form',
    component: LeaveRequestForm,
    render: (args: any) => ({
        components: { LeaveRequestForm },
        setup() {
            return { args };
        },
        template:
            '<LeaveRequestForm :approvedRequestDatesList="args.approvedRequestDatesList" :isErrorOccurred="args.isErrorOccurred" @fetch-new-type-quota="args.fetchQuota" @submit-form="args.submit" :publicDaysList="args.publicDaysList" :leaveTypeItems="args.leaveTypeItems"  />  '
    }),
    tags: ['autodocs'],
    argTypes: {
        leaveTypeItems: {
            name: 'LeaveTypeItems',
            control: 'object',
            type: { name: 'string', required: true },
            defaultValue: ['hello'],
            description: 'The leave type dropdown items data',
            table: {
                type: { summary: 'array', detail: '[ id : number , vacationTypeName : string , vacationYearQuota : number ]' }
            }
        },
        isErrorOccurred: {
            name: 'isErrorOccurred',
            control: 'boolean',
            type: { name: 'boolean', required: true },
            defaultValue: false,
            description: ' Indicating whether an error occured while fetching data'
        },
        publicDaysList: {
            name: 'publicDaysList',
            control: 'object',
            type: { name: 'string', required: true },
            defaultValue: false,
            description: ' An array containing the public days associated with the collaborator country',
            table: {
                type: { summary: 'array', detail: '[  {publicDayDate: string,  id: string} ]' }
            }
        },
        currentVacationDaysQuota: {
            name: 'currentVacationDaysQuota',
            control: 'object',
            type: { name: 'string', required: true },
            defaultValue: undefined,
            description: 'An object containing the current quota for a specific vacation type and a collaborator',
            table: {
                type: { summary: 'object | undefined', detail: '{vacationDaysQuotaId: string , remainingQuota: number}' }
            }
        },
        'fetch-new-type-quota': {
            type: { name: 'string', required: true },
            control: 'object',
            description: 'The event triggered for fetching new vacation quota',
            table: {
                type: { summary: 'event', detail: "(e: 'fetch-new-type-quota' , formValues : vacationFormType) : void" }
            }
        },
        'submit-form': {
            type: { name: 'string', required: true },
            control: 'object',
            description: 'The event that is raised when submitting the form',
            table: {
                type: { summary: 'event', detail: "(e : 'submit-form', values: object) : void" }
            }
        },
        submit: {
            action: 'submit-form'
        },
        fetchQuota: {
            action: 'fetch-new-type-quota'
        }
    }
};

export default meta;

type story = StoryObj<typeof LeaveRequestForm>;

export const Default: story = {
    args: {
        leaveTypeItems: [
            {
                vacationTypeId: '33552',
                vacationTypeName: 'Sick Leave',
                yearlyQuota: 33
            },
            {
                vacationTypeId: '234',
                vacationTypeName: 'Unpaid Leave',
                yearlyQuota: 3
            },
            {
                vacationTypeId: '626258',
                vacationTypeName: 'Vacation',
                yearlyQuota: 50
            }
        ],
        isErrorOccurred: false,
        publicDaysList: [
            {
                id: '253-362',
                publicDayDate: '2023-07-19'
            },
            {
                id: '253-362',
                publicDayDate: '2023-05-01'
            }
        ],
        currentVacationDaysQuota: {
            remainingQuota: 5,
            vacationDaysQuotaId: '25-63256874-63259'
        },
        approvedRequestDatesList: [
            {
                vacationRequestId: '2511',
                dates: ['2023-07-02', '2023-07-05']
            }
        ]
    }
};
export const InvalidForm: story = {
    args: {
        leaveTypeItems: [
            {
                vacationTypeId: '33552',
                vacationTypeName: 'Sick Leave',
                yearlyQuota: 33
            },
            {
                vacationTypeId: '234',
                vacationTypeName: 'Unpaid Leave',
                yearlyQuota: 3
            },
            {
                vacationTypeId: '626258',
                vacationTypeName: 'Vacation',
                yearlyQuota: 50
            }
        ],
        isErrorOccurred: false,
        publicDaysList: [
            {
                id: '253-362',
                publicDayDate: '2023-07-19'
            },
            {
                id: '253-362',
                publicDayDate: '2023-05-01'
            }
        ],
        currentVacationDaysQuota: {
            remainingQuota: 5,
            vacationDaysQuotaId: '25-63256874-63259'
        },
        approvedRequestDatesList: [
            {
                vacationRequestId: '2511',
                dates: ['2023-07-02', '2023-07-05']
            }
        ]
    },
    play: async ({ canvasElement }) => {
        const canvas = within(canvasElement);

        userEvent.click(canvas.getByTestId('leave-request-submit-btn'));
        await sleep(500);
        expect(canvas.getByTestId('leave-request-dropdown-error-msg')).toBeInTheDocument();
        expect(canvas.getByTestId('leave-request-select-btn-start-date-error-msg')).toBeInTheDocument();
        expect(canvas.getByTestId('leave-request-calender-start-error-msg')).toBeInTheDocument();
        expect(canvas.getByTestId('leave-request-select-btn-start-date-error-msg')).toBeInTheDocument();
        expect(canvas.getByTestId('leave-request-calendar-end-date-error-msg')).toBeInTheDocument();
        expect(canvas.getByTestId('leave-request-end-date-select-btn-error-msg')).toBeInTheDocument();
    }
};

export const FilledForm: story = {
    args: {
        leaveTypeItems: [
            {
                vacationTypeId: '33552',
                vacationTypeName: 'Sick Leave',
                yearlyQuota: 33
            },
            {
                vacationTypeId: '234',
                vacationTypeName: 'Unpaid Leave',
                yearlyQuota: 3
            },
            {
                vacationTypeId: '626258',
                vacationTypeName: 'Vacation',
                yearlyQuota: 50
            }
        ],
        isErrorOccurred: false,
        publicDaysList: [
            {
                id: '253-362',
                publicDayDate: '2023-07-19'
            },
            {
                id: '253-362',
                publicDayDate: '2023-05-01'
            }
        ],
        currentVacationDaysQuota: {
            remainingQuota: 5,
            vacationDaysQuotaId: '25-63256874-63259'
        },
        approvedRequestDatesList: [
            {
                vacationRequestId: '2511',
                dates: ['2023-07-02', '2023-07-05']
            }
        ]
    },
    play: async ({ args }) => {
        let date = moment().set('year', new Date().getFullYear()).set('month', new Date().getMonth()).set('date', 1).isoWeekday(8);
        if (date.date() > 7) {
            //
            date = date.isoWeekday(-6);
        }

        const canvas = within(document.getElementsByTagName('body')[0]);
        const select = canvas.getByTestId('leave-request-leave-type-dropdown');
        await userEvent.click(select);
        const dropdownElement = canvas.getByText(args.leaveTypeItems[0].vacationTypeName);
        await userEvent.click(dropdownElement);
        await sleep(500);
        const startDate = await canvas.getByTestId('leave-request-calendar-start-date').firstElementChild;
        if (startDate) await userEvent.click(startDate);
        const selectedStartDate = await canvas.findByText(date.add(1, 'weeks').format('D'));
        await sleep(500);
        if (selectedStartDate) userEvent.click(selectedStartDate);
        await sleep(500);
        const morningRadioStartDate = canvas.getByTestId('leave-request-select-btn-start-date').firstElementChild;
        if (morningRadioStartDate) userEvent.click(morningRadioStartDate);
        await sleep(500);
        const endDate = await canvas.getByTestId('leave-request-calendar-end-date').firstElementChild;
        if (endDate) await userEvent.click(endDate);
        const selectedEndDate = await canvas.findByText(date.add(-3, 'days').format('D'));
        await sleep(500);
        if (selectedEndDate) userEvent.click(selectedEndDate);
        await sleep(500);
        expect(await canvas.getByTestId('leave-request-calendar-end-date').classList.contains('p-invalid')).toBe(true);
        await sleep(1000);
        if (endDate) await userEvent.click(endDate);
        const reSelectedEndDate = await canvas.findByText(date.add(5, 'days').format('D'));
        if (reSelectedEndDate) userEvent.click(reSelectedEndDate);
        await sleep(500);
        expect(await canvas.getByTestId('leave-request-calendar-end-date').classList.contains('p-invalid')).toBe(false);
        await sleep(500);
        const morningRadioEndDate = canvas.getByTestId('leave-request-select-btn-end-date').lastElementChild;
        if (morningRadioEndDate) userEvent.click(morningRadioEndDate);
        await sleep(500);
        const justificationTextArea = canvas.getByTestId('leave-justification-textarea');
        await userEvent.type(justificationTextArea, 'I need to take a 4 days vacation with my family');
        await sleep(500);
        userEvent.click(canvas.getByTestId('leave-request-submit-btn'));
    }
};
