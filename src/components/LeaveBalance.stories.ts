import type { Meta, StoryObj } from '@storybook/vue3';
import LeaveBalance from '@/components/LeaveBalance.vue';
import { within } from '@storybook/testing-library';
import { expect } from '@storybook/jest';
import { sleep } from '@/utils/SleepFunction';
import type { ExtendedVacationDaysQuotaType } from '@/types/vacationDaysQuotaType.';

const meta: Meta<typeof LeaveBalance> = {
    title: 'Leave Balance',
    component: LeaveBalance,
    render: (args: any) => ({
        components: { LeaveBalance },
        setup() {
            return { args };
        },
        template: '<LeaveBalance :leaveBalanceData="args.leaveBalanceData" />'
    }),
    tags: ['autodocs'],
    argTypes: {
        leaveBalanceData: {
            name: 'leaveBalanceData',
            control: 'object',
            type: { name: 'string', required: true },
            description: 'Leave balance array of data',
            table: {
                type: { summary: 'array', detail: '[{ vacationType: string ,used: number,  availavle: number,  allowance: number  }]' }
            }
        }
    }
};

export default meta;

type story = StoryObj<typeof LeaveBalance>;

export const Default: story = {
    args: {
        leaveBalanceData: [
            {
                vacationDaysQuotaId: '255-635261',
                vacationType: { vacationTypeId: '3625-85236-65214-9855', vacationTypeName: 'RTT', yearlyQuota: 30 },
                remainingQuota: 26
            },
            {
                vacationDaysQuotaId: '255-6741261',
                vacationType: { vacationTypeId: '3625-2513985-65214-9855', vacationTypeName: 'RTI', yearlyQuota: 40 },
                remainingQuota: 21
            },
            {
                vacationDaysQuotaId: '255-362261',
                vacationType: { vacationTypeId: '3625-85232514-214-9855', vacationTypeName: 'Sick Leave', yearlyQuota: 30 },
                remainingQuota: 21
            }
        ]
    },
    play: async ({ canvasElement, args }) => {
        const canvas = within(canvasElement);
        await expect(canvas.getByText('My Leaves Balance')).toBeInTheDocument();
        await expect(canvas.getByText('Vacation Type')).toBeInTheDocument();
        await expect(canvas.getByText('Used')).toBeInTheDocument();
        await expect(canvas.getByText('Available')).toBeInTheDocument();
        await expect(canvas.getByText('Permitted')).toBeInTheDocument();

        args.leaveBalanceData.forEach(async (el: ExtendedVacationDaysQuotaType) => {
            await expect(canvas.getByText(el.vacationType.vacationTypeName)).toBeInTheDocument();
            sleep(500);
            await expect(canvas.getAllByText(el.vacationType.yearlyQuota)[0]).toBeInTheDocument();
            sleep(500);
            await expect(canvas.getAllByText(`${el.vacationType.yearlyQuota - el.remainingQuota}`)[0]).toBeInTheDocument();
            sleep(500);
            await expect(canvas.getAllByText(el.remainingQuota)[0]).toBeInTheDocument();
        });
    }
};
