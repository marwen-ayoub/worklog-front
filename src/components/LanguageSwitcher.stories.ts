import { expect } from '@storybook/jest';
import type { Meta, StoryObj } from '@storybook/vue3';
import LanguageSwitcher from '@/components/LanguageSwitcher.vue';
import { within } from '@storybook/testing-library';

const meta: Meta<typeof LanguageSwitcher> = {
    title: 'Language Switcher',
    component: LanguageSwitcher,
    render: (args: any) => ({
        components: { LanguageSwitcher },
        setup() {
            return { args };
        },
        template: "<LanguageSwitcher  @update:modelValue='args.update'  v-model='args.initialLanguage' :availableLanguages='args.availableLanguages' />"
    }),
    tags: ['autodocs'],
    argTypes: {
        modelValue: {
            name: 'modelValue',
            type: { name: 'string', required: true },
            description: 'A string containing the current language',
            control: 'select',
            options: ['fr', 'en']
        },
        availableLanguages: {
            name: 'availableLanguages',
            control: 'string',
            type: { name: 'string', required: true },
            description: 'An array strings containing the available languages',
            table: {
                type: { summary: 'Array' }
            }
        }
    }
};

export default meta;

type story = StoryObj<typeof LanguageSwitcher>;

export const Default: story = {
    args: {
        availableLanguages: ['fr', 'en']
    },
    play: async ({ canvasElement }) => {
        const canvas = within(canvasElement);
        const initialFlagBtn = await canvas.findByTestId('flag-btn');
        await expect(initialFlagBtn).toBeInTheDocument();
    }
};
