import type { Meta, StoryObj } from '@storybook/vue3';
import AddTimesheet from '@/components/AddTimesheet.vue';
import { userEvent, within } from '@storybook/testing-library';

import { expect } from '@storybook/jest';
import { sleep } from '@/utils/SleepFunction';
import { ROLE, VACATION_TIME } from '@/constants/constant';
import moment from 'moment';

const meta: Meta<typeof AddTimesheet> = {
    title: 'Timesheet Component',
    component: AddTimesheet,

    render: (args: any, context) => ({
        components: { AddTimesheet },
        setup() {
            const weekDates = Array.from({ length: 5 }, (_, i) => moment().startOf('week').clone().add(i, 'days'));
            const formattedWeekDates: string[] = Array.from({ length: 5 }, (_, i) => moment().startOf('week').clone().add(i, 'days').format('yyyy-MM-DD'));
            function handleChangeLoadingState() {
                setTimeout(() => {
                    args.isFirstFetchDone = true;
                    args.isSecondFetchDone = true;
                    args.isErrorOccurred = false;
                }, 700);
            }
            handleChangeLoadingState();
            setTimeout(() => {
                args.userRole = context.story === 'Manager Validation' ? ROLE.MANAGER : ROLE.COLLABORATOR;
                args.month = null;
                args.year = null;
                args.publicHoliday = [
                    {
                        id: 1,
                        publicDayDate: '2023-01-14'
                    },
                    {
                        id: 2,
                        publicDayDate: '2023-06-02'
                    },
                    {
                        id: 3,
                        publicDayDate: '2023-07-15'
                    },
                    {
                        id: 4,
                        publicDayDate: '2023-07-18'
                    }
                ];

                args.imputationEntriesRawData = [
                    {
                        assignationId: 2,
                        project: {
                            projectName: 'workLog',
                            projectId: 2
                        },
                        startDate: '2020-01-03',
                        endDate: '2023-10-28',
                        timesheetEntries: [
                            {
                                id: 24,
                                entryDate: formattedWeekDates[0],
                                entryValue: 6
                            },
                            {
                                id: 25,
                                entryDate: formattedWeekDates[1],
                                entryValue: 8
                            },
                            {
                                id: 26,
                                entryDate: formattedWeekDates[2],
                                entryValue: 8
                            }
                        ]
                    }
                ];
                args.offProjects = [
                    {
                        assignationId: 5,
                        startDate: '2023-02-10',
                        endDate: null,
                        project: {
                            projectName: 'Authorisation',
                            projectId: 4
                        }
                    },
                    {
                        assignationId: 88,
                        startDate: '2023-02-10',
                        project: {
                            projectName: 'Intercontrat',
                            projectId: 5
                        }
                    },
                    {
                        assignationId: 6,
                        startDate: '2023-02-10',
                        project: {
                            projectName: 'Formation',
                            projectId: 2
                        }
                    }
                ];
                args.monthlyTimesheetValidationResponse = {
                    success: true,
                    invalidTimesheetEntries: []
                };
                args.formattedMonthlyStatus = [
                    {
                        timesheetMonth: 6,
                        timesheetYear: 2023,
                        monthStatus: 'APPROVED'
                    },
                    {
                        timesheetMonth: 8,
                        timesheetYear: 2023,
                        monthStatus: 'PENDING'
                    },
                    {
                        timesheetMonth: 7,
                        timesheetYear: 2023,
                        monthStatus: 'REJECTED'
                    }
                ];

                args.approvedVacationRequestDatesList = [
                    {
                        vacationRequestId: 'ddmpmgr',
                        vacationRequestType: 'RTT',
                        dates: [
                            new Date(
                                moment(weekDates[4])
                                    .set({ hour: VACATION_TIME.EVENING - 1, minutes: 0 })
                                    .format('YYYY-MM-DDTHH:mm:ssZ')
                            ),
                            new Date(moment(weekDates[3]).set({ hour: VACATION_TIME.MORNING, minutes: 0 }).format('YYYY-MM-DDTHH:mm:ssZ'))
                        ]
                    }
                ];
            }, 100);
            return { args, weekDates };
        },
        template:
            '<AddTimesheet :approved-vacation-request-dates-list="args.approvedVacationRequestDatesList" :monthly-timesheet-validation-response="args.monthlyTimesheetValidationResponse" :isErrorOccurred="args.isErrorOccurred" :user-role="args.userRole" :month="args.month" :year="args.year" :public-holiday="args.publicHoliday" :formatted-monthly-status="args.formattedMonthlyStatus" :off-projects="args.offProjects" :imputation-entries-raw-data="args.imputationEntriesRawData" :is-second-fetch-done="args.isSecondFetchDone" :is-first-fetch-done="args.isFirstFetchDone" @send-month-for-approval="args.sendMonthForApproval"  @fetch-new-week="args.fetchNewWeek" @save-draft-weekly-imputation="args.saveDraft" @export-timesheet="args.exportTimesheet" @send-manager-response="args.sendManagerResponse"   />'
    }),

    tags: ['autodocs'],
    argTypes: {
        imputationEntriesRawData: {
            name: 'imputationEntriesRawData',
            control: 'object',
            type: { name: 'string', required: true },
            description: 'An array of imputation entries raw data',
            table: {
                type: {
                    summary: 'Array',
                    detail: '[ { id: number, project: Project, imputationEntry: WeeklyTimesheet}]'
                },
                defaultValue: { summary: '[]' }
            }
        },
        formattedMonthlyStatus: {
            name: 'formattedMonthlyStatus',
            control: 'object',
            type: { name: 'string', required: true },
            description: 'An array containing monthly imputation status',
            table: {
                type: {
                    summary: 'Array',
                    detail: '[{ timesheetMonth: string, monthStatus: MonthlyStatus}]'
                },
                defaultValue: { summary: '[]' }
            }
        },
        'fetch-new-week': {
            name: 'fetch-new-week',
            control: 'object',
            type: { name: 'string', required: true },
            description: 'An event is triggered when a we switch between weeks',
            table: {
                type: {
                    summary: 'Event',
                    detail: "(e: 'fetch-new-week', startDayOfWeek: string, endDayOfWeek: string, isoDaysOfWeek: string[]): void"
                }
            }
        },
        'save-draft-weekly-imputation': {
            name: 'save-draft-weekly-imputation',
            control: 'object',
            type: { name: 'string', required: true },
            description: 'An event is triggered when a we save weekly timesheet',
            table: {
                type: {
                    summary: 'Event',
                    detail: "(e: 'save-draft-weekly-imputation', weeklyDraftData: ImputationTimesheetDraft[]): void"
                }
            }
        },
        publicHoliday: {
            name: 'publicHoliday',
            control: 'object',
            type: { name: 'string', required: true },
            description: 'An array containing public holidays',
            table: {
                type: {
                    summary: 'String[]'
                }
            }
        },
        approvedVacationRequestDatesList: {
            name: 'approvedVacationRequestDatesList',
            control: 'object',
            type: { name: 'string', required: true },
            description: 'An array containing approved vacation request list of dates',
            table: {
                type: {
                    summary: 'Array',
                    detail: '[{ vacationRequestId: UUID, vacationTypeName: String,  dates: Date[]}]'
                },
                defaultValue: { summary: '[]' }
            }
        },
        isFirstFetchDone: {
            name: 'isFirstFetchDone',
            control: 'boolean',
            type: { name: 'boolean', required: true },
            description: 'A prop controlling the initial skeleton loading',
            table: {
                type: {
                    summary: 'boolean'
                }
            },
            defaultValue: { summary: 'false' }
        },
        isSecondFetchDone: {
            name: 'isSecondFetchDone',
            control: 'boolean',
            type: { name: 'boolean', required: true },
            description: 'A prop controlling the loading indicator after rendering the component',
            table: {
                type: {
                    summary: 'boolean'
                }
            },
            defaultValue: { summary: 'false' }
        },
        isErrorOccurred: {
            name: 'isErrorOccurred',
            control: 'boolean',
            type: { name: 'boolean', required: true },
            description: 'A prop controlling whether an error occurred while fetching data',
            table: {
                type: {
                    summary: 'boolean'
                }
            },
            defaultValue: { summary: 'false' }
        },
        offProjects: {
            name: 'offProjects',
            control: 'object',
            type: { name: 'string', required: true },
            description: 'An array containing off-projects type of projects',
            table: {
                type: {
                    summary: 'String[]'
                }
            },
            defaultValue: { summary: '[]' }
        },
        month: {
            name: 'month',
            type: { name: 'number', required: false },
            description: 'The month number to be displayed in the current timesheet',
            table: {
                type: {
                    summary: 'number'
                }
            }
        },
        year: {
            name: 'year',
            type: {
                name: 'number',
                required: false
            },
            description: 'The year number to be displayed in the current timesheet',
            table: {
                type: {
                    summary: 'number'
                }
            }
        },
        monthlyTimesheetValidationResponse: {
            name: 'monthlyTimesheetValidationResponse',
            control: 'object',
            type: { name: 'string', required: true },
            description: 'An array that stores the daily validation errors for the selected month',
            table: {
                type: {
                    summary: 'Array',
                    detail: '[{ success: boolean, invalidTimesheetEntries: MonthlyInvalidEntryType[]}]'
                },
                defaultValue: { summary: '[]' }
            }
        },
        'send-month-for-approval': {
            name: 'send-month-for-approval',
            control: 'object',
            type: { name: 'string', required: true },
            description: 'An event triggered when a month is to be send for approval',
            table: {
                type: {
                    summary: 'Event',
                    detail: "(e: 'send-month-for-approval', lastWeekOfMonth: LastWeekOfMonthEntries[], monthNumber: number): void"
                }
            }
        },
        'export-timesheet': {
            name: 'export-timesheet',
            control: 'object',
            type: { name: 'string', required: true },
            description: 'An event triggered when a timesheet is to be exported',
            table: {
                type: {
                    summary: 'Event',
                    detail: "(e: 'export-timesheet'): void"
                }
            }
        },
        'send-manager-response': {
            name: 'send-manager-response',
            control: 'object',
            description: 'An event triggered when the manager approve/reject the monthly timesheet',
            table: {
                type: {
                    summary: 'Event',
                    details: "(e: 'managerResponse: 'APPROVED' | 'REJECTED', imputationRequestId: string | null | undefined, startDayOfWeek: string, endDayOfWeek: string, month: number, year: number) : void'"
                }
            }
        },

        exportTimesheet: {
            action: 'export-timesheet'
        },
        sendMonthForApproval: {
            action: 'send-month-for-approval'
        },
        saveDraft: {
            action: 'save-draft-weekly-imputation'
        },
        fetchNewWeek: {
            action: 'fetch-new-week'
        },
        sendManagerResponse: {
            action: 'send-manager-response'
        }
    }
};

export default meta;

type story = StoryObj<typeof AddTimesheet>;

export const FetchedTimesheetComponent: story = {
    args: {
        isFirstFetchDone: false,
        isSecondFetchDone: false,
        isErrorOccurred: false,
        userRole: ROLE.COLLABORATOR,
        imputationEntriesRawData: [
            {
                assignationId: 2,
                project: {
                    projectName: 'Intercontrat',
                    projectId: 2
                },
                startDate: '2020-01-03',
                endDate: '2023-10-28',
                timesheetEntries: [
                    {
                        entryDate: '2023-08-24',
                        entryValue: 8
                    },
                    {
                        entryDate: '2023-08-26',
                        entryValue: 8
                    }
                ]
            },
            {
                assignationId: 5,
                project: {
                    projectName: 'Projet Interne',
                    projectId: 5
                },
                startDate: '2020-01-03',
                endDate: '2024-10-15',
                timesheetEntries: [
                    {
                        id: 24,
                        entryDate: '2023-08-27',
                        entryValue: 8
                    },
                    {
                        id: 25,
                        entryDate: '2023-08-28',
                        entryValue: 8
                    }
                ]
            }
        ],
        formattedMonthlyStatus: [
            {
                timesheetMonth: 7,
                timesheetYear: 2023,
                monthStatus: 'PENDING'
            },
            {
                timesheetMonth: 8,
                timesheetYear: 2023,
                monthStatus: 'APPROVED'
            },
            {
                timesheetMonth: 9,
                timesheetYear: 2023,
                monthStatus: 'APPROVED'
            }
        ],
        approvedVacationRequestDatesList: [
            {
                vacationRequestId: 'ddmpmgr',
                vacationRequestType: 'RTT',
                dates: ['2023-09-06T08:00:00.630Z', '2023-09-07T08:00:00.630Z']
            }
        ],
        publicHoliday: [
            {
                id: 1,
                publicDayDate: '2023-01-14'
            },
            {
                id: 2,
                publicDayDate: '2023-06-02'
            },
            {
                id: 3,
                publicDayDate: '2023-07-03'
            },
            {
                id: 4,
                publicDayDate: '2023-06-18'
            }
        ],
        offProjects: [
            {
                assignationId: 5,
                startDate: '2023-01-01',
                endDate: null,
                project: {
                    projectName: 'Off Project',
                    projectId: 4
                }
            },
            {
                assignationId: 88,
                startDate: '2023-01-01',
                endDate: null,
                project: {
                    projectName: 'Not Billable',
                    projectId: 5
                }
            },
            {
                assignationId: 6,
                startDate: '2023-01-01',
                endDate: null,
                project: {
                    projectName: 'Training',
                    projectId: 2
                }
            }
        ]
    },
    play: async () => {
        await sleep(1000);
        const canvas = within(document.getElementsByTagName('body')[0]);
        const addTimesheetBtn = canvas.getByTestId('add-timesheet-row');
        userEvent.click(addTimesheetBtn);
        await sleep(400);
        const dropdown = canvas.getByTestId('project-dropdown');
        userEvent.click(dropdown);
        await sleep(400);
        const option = canvas.getByText('Authorisation');
        userEvent.click(option);
        await sleep(400);
        const confirmAddBtn = canvas.getByTestId('confirm-add-timesheet-row-btn');
        userEvent.click(confirmAddBtn);
        await sleep(2000);
        expect(await canvas.getByTestId('project-name-span-1').textContent).toBe('Authorisation');
        await sleep(600);
        const mondayInput2 = canvas.getByTestId('monday-input-1').firstElementChild as HTMLElement;
        const wednesdayInput = canvas.getByTestId('wednesday-input-0').firstElementChild as HTMLElement;
        const fridayInput = canvas.getByTestId('friday-input-0').firstElementChild as HTMLElement;
        await sleep(300);
        await userEvent.click(mondayInput2);
        await userEvent.type(mondayInput2, '2');
        await sleep(300);
        const totalWeeklyHours = canvas.getByTestId('total-week-hours');
        userEvent.click(totalWeeklyHours);
        await sleep(300);
        expect(await canvas.getByTestId('wednesday-input-0').classList.contains('p-invalid')).toBe(true);
        await userEvent.click(wednesdayInput);
        await userEvent.type(wednesdayInput, '4');
        await sleep(500);
        const draftBtn = canvas.getByTestId('save-draft-btn');
        expect(draftBtn).toBeDisabled();
        await userEvent.click(fridayInput);
        await userEvent.type(fridayInput, '8');
        await sleep(500);
        userEvent.click(totalWeeklyHours);
        await sleep(100);
        expect(draftBtn).not.toBeDisabled();
    }
};
export const ManagerValidation: story = {
    args: {
        isFirstFetchDone: false,
        isSecondFetchDone: false,
        isErrorOccurred: false,
        userRole: ROLE.MANAGER,
        imputationEntriesRawData: [],
        publicHoliday: [],
        offProjects: [],
        approvedVacationRequestDatesList: [],
        formattedMonthlyStatus: []
    }
};
