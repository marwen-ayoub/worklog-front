import NextLeavesList from '@/components/NextLeavesList.vue';
import { sleep } from '@/utils/SleepFunction';
import { expect } from '@storybook/jest';
import { within } from '@storybook/testing-library';
import type { Meta, StoryObj } from '@storybook/vue3';
import { handleNexTLeaveStatusText } from '@/utils/StatusTagSeverity';
import type { VacationRequestType } from '@/types/vacationRequestType';
import moment from 'moment';

const meta: Meta<typeof NextLeavesList> = {
    title: 'Next Leave List Component',
    component: NextLeavesList,
    render: (args: any) => ({
        components: { NextLeavesList },
        setup() {
            return { args };
        },
        template: '<NextLeavesList :ImputationList="args.ImputationList" />'
    }),
    tags: ['autodocs'],
    argTypes: {
        ImputationList: {
            name: 'ImputationList',
            control: 'object',
            type: { name: 'string', required: true },
            description: 'The next leaves array of data',
            table: {
                type: { summary: 'array', detail: '[{  id: number,nbOfDays: number,startDate: Date,endDate: Date,vacationType: string,status: Status;  }]' }
            }
        }
    }
};

export default meta;

type story = StoryObj<typeof NextLeavesList>;

export const Default: story = {
    args: {
        ImputationList: [
            {
                vacationRequestId: '25-6232325-632255',
                comment: "I want to get a vacation because i'm sick",
                nbOfDays: 1,
                collaborator: { collaboratorId: '25863', firstName: 'Marwen', lastName: 'Ayoub', role: 'Collaborator' },
                manager: { collaboratorId: '25863', firstName: 'Aymen', lastName: 'HOSNI', role: 'Manager' },
                startDate: '2023-02-15',
                endDate: '2023-02-16',
                vacationType: { vacationTypeId: '325-6325-6325', vacationTypeName: 'Sick Leave', yearlyQuota: 25 },
                status: 'APPROVED'
            },
            {
                vacationRequestId: '25-6232325-632255',
                nbOfDays: 3,
                collaborator: { collaboratorId: '25863', firstName: 'Marwen', lastName: 'Ayoub', role: 'Collaborator' },
                manager: { collaboratorId: '25863', firstName: 'Aymen', lastName: 'HOSNI', role: 'Manager' },
                startDate: '2023-03-03',
                endDate: '2023-03-07',
                vacationType: { vacationTypeId: '325-6325-6325', vacationTypeName: 'Vacation', yearlyQuota: 25 },
                status: 'PENDING'
            }
        ]
    },
    play: async ({ canvasElement, args }) => {
        const canvas = within(canvasElement);

        let nbOfDaysText;
        args.ImputationList.forEach(async (el: VacationRequestType) => {
            await expect(canvas.getByText(el.vacationType.vacationTypeName)).toBeInTheDocument();
            await sleep(500);
            await expect(canvas.getAllByText(moment(el.startDate).format('DD/MM/yyyy') + ' - ' + moment(el.endDate).format('DD/MM/yyyy'))[0]).toBeInTheDocument();
            await sleep(500);
            if (el.nbOfDays > 1) {
                nbOfDaysText = el.nbOfDays + ' Days';
            } else {
                nbOfDaysText = 'One Day';
            }
            await expect(canvas.getByText(nbOfDaysText)).toBeInTheDocument();
            await sleep(500);
            await expect(canvas.getByText(handleNexTLeaveStatusText(el.status))).toBeInTheDocument();
        });
    }
};
