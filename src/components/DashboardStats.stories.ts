import type { Meta, StoryObj } from '@storybook/vue3';

import DashboardStats from '@/components/DashboardStats.vue';
import { within } from '@storybook/testing-library';
import { expect } from '@storybook/jest';
import { sleep } from '@/utils/SleepFunction';

const meta: Meta<typeof DashboardStats> = {
    title: 'Dashboard stats',
    component: DashboardStats,
    render: (args: any) => ({
        components: { DashboardStats },
        setup() {
            return { args };
        },
        template: '<DashboardStats :name="args.name" :color="args.color" :status="args.status" :nb="args.nb" />'
    }),
    argTypes: {
        name: {
            description: 'The main title of the stats component'
        },
        color: {
            description: 'The background color of the stats component'
        },
        status: {
            description: 'The status of the stats'
        },
        nb: {
            description: 'The number of the stats to be measured'
        }
    },
    tags: ['autodocs']
};

export default meta;

type story = StoryObj<typeof DashboardStats>;

export const Default: story = {
    args: {
        name: 'Leave Types',
        color: 'red',
        nb: 4,
        status: 'Pending approval'
    },
    play: async ({ canvasElement, args }) => {
        const canvas = within(canvasElement);
        await expect(canvas.getByText(args.name)).toBeInTheDocument();
        if (args.nb) await expect(canvas.getByText(args.nb)).toBeInTheDocument();
        await expect(canvas.getByText(args.status)).toBeInTheDocument();
        sleep(500);
        const backgroundDiv = await canvas.getByTestId('dashboard-stats-icon-div-bg');
        await expect(backgroundDiv.classList.contains('bg-' + args.color + '-100')).toBe(true);
        sleep(500);
        const iconColor = await canvas.getByTestId('dashboard-stats-icon-color');
        await expect(iconColor.classList.contains('text-' + args.color + '-500')).toBe(true);
    }
};
