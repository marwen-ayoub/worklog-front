<script setup lang="ts">
import { computed, ref, watch } from 'vue';
import moment, { type Moment } from 'moment';

import * as yup from 'yup';

import Dropdown from 'primevue/dropdown';
import Button from 'primevue/button';
import Calendar from 'primevue/calendar';
import SelectButton from 'primevue/selectbutton';
import Textarea from 'primevue/textarea';

import type { vacationFormType } from '@/types/types';
import type { vacationType } from '@/types/vacationType';
import type { VacationDaysQuotaType } from '@/types/vacationDaysQuotaType.';
import type { PublicDayType } from '@/types/countryType';
import type { CalendarMonthChangeEvent } from 'primevue/calendar';

import { VACATION_TIME } from '@/constants/constant';

import { useField, useForm } from 'vee-validate';
import { toTypedSchema } from '@vee-validate/yup';
import { useI18n } from 'vue-i18n';
import type { VacationRequestDatesList } from '@/types/vacationRequestType';

const { t } = useI18n();

type SelectOption = {
    name: string;
    value: boolean;
    disabled: boolean;
};

type modifiedDateType = {
    startDate: Date | null;
    endDate: Date | null;
};

const schema = toTypedSchema(
    yup.object({
        vacationType: yup.object().required(() => t('VacationTypeLeaveErrorMessage')),
        startDate: yup.date().required(() => t('StartingDateErrorMessage')),
        endDate: yup
            .date()
            .required(() => t('EndingDateErrorMessage'))
            .min(yup.ref('startDate'), () => t('EndDateBeforeStartDateErrorMessage')),
        morningStartDate: yup.boolean().required(() => t('StartDatePeriod')),
        morningEndDate: yup.boolean().required(() => t('EndingDatePeriod')),
        comment: yup.string()
    })
);

type propType = {
    leaveTypeItems: vacationType[];
    isErrorOccurred: boolean;
    currentVacationDaysQuota: VacationDaysQuotaType | undefined;
    publicDaysList: PublicDayType[];
    approvedRequestDatesList: VacationRequestDatesList[];
};

/**
 * * Define Props
 */

const props = defineProps<propType>();

/**
 * * Define Emits
 */

const emits = defineEmits<{
    (e: 'submit-form', formValues: vacationFormType): void;
    (e: 'fetch-new-type-quota', vacationTypeId: string): void;
    (e: 'fetch-approved-vacation-dates', vacationTypeId: string, startDate: string, endDate: string): void;
}>();

/**
 * * Reactive Variables
 */
const modifiedDate = ref<modifiedDateType>({
    startDate: null,
    endDate: null
});
const vacationQuotaIfApproved = ref<number>();
const approvedDaysList = ref<Date[]>([]);
const filteredDaysList = ref<Date[]>([]);
const currentSelectedMonth = ref<number>();
const currentSelectedYear = ref<number>();

/**
 * * Composable
 */

const { handleSubmit, isSubmitting } = useForm({
    validationSchema: schema
});

const { value: selectedVacationType, errorMessage: vacationTypeError } = useField<vacationType>('vacationType');
const { value: startDate, errorMessage: startDateError } = useField<Date>('startDate');
const { value: endDate, errorMessage: endDateError } = useField<Date>('endDate');
const { value: isMorningStartDate, errorMessage: morningStartDateError } = useField<boolean>('morningStartDate');
const { value: isMorningEndDate, errorMessage: morningEndDateError } = useField<boolean>('morningEndDate');
const { value: comment, errorMessage: commentError } = useField<string | undefined>('comment');

/**
 * * Watchers
 */

watch([startDate, endDate, isMorningStartDate, isMorningEndDate], ([newStartDate, newEndDate, newIsMorningStartDate, newIsMorningEndDate]) => {
    handleDateChange(newStartDate, newEndDate, newIsMorningStartDate, newIsMorningEndDate);
});

watch([() => props.currentVacationDaysQuota, startDate, endDate, isMorningEndDate, isMorningEndDate], ([newVacationDaysQuota]) => {
    if (moment(startDate.value).isSameOrBefore(endDate.value)) {
        if (newVacationDaysQuota && startDate.value !== undefined && endDate.value !== undefined && isMorningEndDate !== undefined && isMorningStartDate !== undefined) calculateNumberOfDaysIfApproved(newVacationDaysQuota.remainingQuota);
    } else {
        vacationQuotaIfApproved.value = undefined;
    }
});

watch([selectedVacationType, () => props.approvedRequestDatesList], () => {
    disabledDays();
});

/**
 * * Computed
 */

const startDateOptions = ref<SelectOption[]>([
    { name: t('MorningText'), value: true, disabled: false },
    { name: t('EveningText'), value: false, disabled: false }
]);

const endDateOptions = ref<SelectOption[]>([
    { name: t('MorningText'), value: true, disabled: false },
    { name: t('EveningText'), value: false, disabled: false }
]);

const getOptions = (options: SelectOption[]) => options.map((option) => ({ ...option }));

const computedStartDateOptions = computed({
    get: () => getOptions(startDateOptions.value),
    set: (newDisabledOptions) => {
        startDateOptions.value = startDateOptions.value.map((option) => {
            const matchingOption = newDisabledOptions.find((disabledOption) => disabledOption.name === option.name);
            if (matchingOption) {
                return {
                    ...option,
                    disabled: matchingOption.disabled
                };
            }
            return option;
        });
    }
});

const computedEndDateOptions = computed({
    get: () => getOptions(endDateOptions.value),
    set: (newDisabledOptions) => {
        endDateOptions.value = endDateOptions.value.map((option) => {
            const matchingOption = newDisabledOptions.find((disabledOption) => disabledOption.name === option.name);
            if (matchingOption) {
                return {
                    ...option,
                    disabled: matchingOption.disabled
                };
            }
            return option;
        });
    }
});

const disabledDays = () => {
    const filteredDisabledDaysList: Date[] = props.approvedRequestDatesList
        .map((requestObject) => {
            return {
                ...requestObject,
                dates: requestObject.dates.filter((date, index) => {
                    if (index === 0 && moment(date).hour() === VACATION_TIME.EVENING) return false;
                    else if (index === requestObject.dates.length - 1 && moment(date).hour() === VACATION_TIME.MORNING) return false;
                    return true;
                })
            };
        })
        .flatMap((requestObject) => requestObject.dates)
        .map((el) => new Date(el));
    const formattedDisabledDaysList: Date[] = props.approvedRequestDatesList.flatMap((requestObject) => requestObject.dates).map((el) => new Date(el));
    const list = [...new Set(props.publicDaysList.map((publicDay) => new Date(publicDay.publicDayDate)).concat(filteredDisabledDaysList))];
    approvedDaysList.value = formattedDisabledDaysList.sort((date1: Date, date2: Date) => new Date(date1).setHours(0, 0, 0, 0) - new Date(date2).setHours(0, 0, 0, 0));
    filteredDaysList.value = filteredDisabledDaysList;
    return list;
};
const currentQuota = computed(() => {
    return props.currentVacationDaysQuota ? t('AllowanceDays', props.currentVacationDaysQuota.remainingQuota) : '- -';
});

const ifApprovedQuota = computed(() => {
    console.log(vacationQuotaIfApproved.value);
    return typeof vacationQuotaIfApproved.value === 'number' ? t('RestingDays', vacationQuotaIfApproved.value) : '- -';
});

const isQuotaNegative = computed(() => {
    if (vacationQuotaIfApproved.value !== undefined) return vacationQuotaIfApproved.value < 0;
    return false;
});

const isCalenderDisabled = computed(() => {
    return !selectedVacationType.value;
});

/**
 * * Methods
 */

const checkIsPublicDay = (day: Moment): boolean => {
    return props.publicDaysList.some((publicDay: PublicDayType) => moment(publicDay.publicDayDate).isSame(day, 'day'));
};

const calculateNumberOfDaysIfApproved = (currentQuota: number) => {
    const vacationRequestNumberOfDays = adjustNbDayBasedOnMorningOrAfternoon();
    if (typeof vacationRequestNumberOfDays === 'number') {
        vacationQuotaIfApproved.value = currentQuota - vacationRequestNumberOfDays;
    } else vacationQuotaIfApproved.value = undefined;
};

const calculateVacationRequestNumberOfDays = () => {
    let currentDay = moment(startDate.value);
    let nbOfDays: number = 0;
    while (currentDay.isBefore(moment(endDate.value))) {
        if (moment(currentDay).weekday() !== 0 && moment(currentDay).weekday() !== 6 && !checkIsPublicDay(currentDay)) {
            nbOfDays += 1;
        }
        currentDay = moment(currentDay).add(1, 'day');
    }
    return nbOfDays + 1;
};

const adjustNbDayBasedOnMorningOrAfternoon = () => {
    let nbOfDays = calculateVacationRequestNumberOfDays();
    const isMorningStart = isMorningStartDate.value;
    const isMorningEnd = isMorningEndDate.value;
    if (typeof isMorningStart === 'boolean' && typeof isMorningEnd === 'boolean') {
        if (isMorningStart === isMorningEnd) {
            nbOfDays -= 0.5;
        } else if ((!isMorningStart && isMorningEnd) || (isMorningStart && !isMorningEnd)) {
            nbOfDays -= 1;
        }
        return nbOfDays;
    } else return undefined;
};

const onSubmit = handleSubmit((values: vacationFormType) => {
    emits('submit-form', values);
});

const handleUpdateStartDateAndEndDate = () => {
    if (!moment(startDate.value).isSame(modifiedDate.value.startDate)) {
        if (modifiedDate.value.startDate !== undefined && modifiedDate.value.startDate !== null) {
            startDate.value = modifiedDate.value.startDate;
        }
    }
    if (!moment(endDate.value).isSame(modifiedDate.value.endDate)) {
        if (modifiedDate.value.endDate !== undefined && modifiedDate.value.endDate !== null) {
            endDate.value = modifiedDate.value.endDate;
        }
    }
};

const handleVacationTypeChange = (value: vacationType) => {
    emits('fetch-new-type-quota', value.vacationTypeId);
};

const handleMonthChange = (calendarMonth: CalendarMonthChangeEvent) => {
    if (selectedVacationType.value.vacationTypeId) {
        emits(
            'fetch-approved-vacation-dates',
            selectedVacationType.value.vacationTypeId,
            moment({ year: calendarMonth.year, month: calendarMonth.month - 1 })
                .startOf('month')
                .format('YYYY-MM-DD'),
            moment({ year: calendarMonth.year, month: calendarMonth.month - 1 })
                .endOf('month')
                .format('YYYY-MM-DD')
        );
        currentSelectedMonth.value = calendarMonth.month - 1;
        currentSelectedYear.value = calendarMonth.year;
    }
};

const handleDateChange = (newStartDate: Date, newEndDate: Date, newIsMorningStartDate: boolean, newIsMorningEndDate: boolean) => {
    const morningHour: number = VACATION_TIME.MORNING;
    const eveningHour: number = VACATION_TIME.EVENING;
    const minute: number = 0;

    const modifiedDateValues: modifiedDateType = { ...modifiedDate.value };

    if (newStartDate !== undefined && newIsMorningStartDate !== undefined) {
        if (newIsMorningStartDate === true) {
            modifiedDateValues.startDate = new Date(moment(newStartDate).set({ hour: morningHour, minute }).format('YYYY-MM-DDTHH:mm:ssZ'));
        } else if (newIsMorningStartDate === false) {
            modifiedDateValues.startDate = new Date(moment(newStartDate).set({ hour: eveningHour, minute }).format('YYYY-MM-DDTHH:mm:ssZ'));
        }
    }
    if (newEndDate !== undefined && newIsMorningEndDate !== undefined) {
        if (newIsMorningEndDate === true) {
            modifiedDateValues.endDate = new Date(moment(newEndDate).set({ hour: morningHour, minute }).format('YYYY-MM-DDTHH:mm:ssZ'));
        } else if (newIsMorningEndDate === false) {
            modifiedDateValues.endDate = new Date(moment(newEndDate).set({ hour: eveningHour, minute }).format('YYYY-MM-DDTHH:mm:ssZ'));
        }
    }
    modifiedDate.value = modifiedDateValues;
    handleUpdateStartDateAndEndDate();
};

const handleGetInitialMonthDisabledDays = () => {
    if (selectedVacationType.value.vacationTypeId) {
        if (typeof currentSelectedMonth.value === 'number' && typeof currentSelectedYear.value === 'number') {
            emits(
                'fetch-approved-vacation-dates',
                selectedVacationType.value.vacationTypeId,
                moment({ month: currentSelectedMonth.value, year: currentSelectedYear.value }).startOf('month').format('YYYY-MM-DD'),
                moment({ month: currentSelectedMonth.value, year: currentSelectedYear.value }).endOf('month').format('YYYY-MM-DD')
            );
        } else {
            emits('fetch-approved-vacation-dates', selectedVacationType.value.vacationTypeId, moment().startOf('month').format('YYYY-MM-DD'), moment().endOf('month').format('YYYY-MM-DD'));
        }
    }
};

const updateStartDateModelValue = (startDateValue: string) => {
    startDateOptions.value[0].disabled = false;
    startDateOptions.value[1].disabled = false;
    computedStartDateOptions.value = startDateOptions.value;

    approvedDaysList.value.some((approvedDay, index) => {
        if (moment(approvedDay).isSame(startDateValue, 'day')) {
            if (approvedDaysList.value.length === 1) {
                if (moment(approvedDay).hour() === VACATION_TIME.MORNING) {
                    isMorningStartDate.value = false;
                    startDateOptions.value[0].disabled = true;
                    startDateOptions.value[1].disabled = false;
                    return true;
                } else if (moment(approvedDay).hour() === VACATION_TIME.EVENING) {
                    isMorningStartDate.value = true;
                    startDateOptions.value[0].disabled = false;
                    startDateOptions.value[1].disabled = true;
                    return true;
                }
            } else {
                const occuranceOfDays = approvedDaysList.value.reduce((acc, currentDay) => {
                    if (moment(currentDay).isSame(startDateValue, 'day')) acc++;
                    return acc;
                }, 0);
                if (occuranceOfDays === 2) {
                    startDateOptions.value[0].disabled = true;
                    startDateOptions.value[1].disabled = true;
                    return true;
                } else if (occuranceOfDays === 1) {
                    if (
                        moment(approvedDay)
                            .subtract(1, 'day')
                            .isSame(approvedDaysList.value[index - 1])
                    ) {
                        if (moment(approvedDay).hour() === VACATION_TIME.MORNING && (moment(approvedDaysList.value[index - 1]).hour() === VACATION_TIME.EVENING || moment(approvedDaysList.value[index - 1]).hour() === VACATION_TIME.MORNING)) {
                            isMorningStartDate.value = false;
                            startDateOptions.value[0].disabled = true;
                            startDateOptions.value[1].disabled = false;
                            computedStartDateOptions.value = startDateOptions.value;
                            return true;
                        }
                    } else {
                        if (moment(approvedDay).hour() === VACATION_TIME.MORNING) {
                            isMorningStartDate.value = false;
                            startDateOptions.value[0].disabled = true;
                            startDateOptions.value[1].disabled = false;
                            computedStartDateOptions.value = startDateOptions.value;
                            return true;
                        } else if (moment(approvedDay).hour() === VACATION_TIME.EVENING) {
                            isMorningStartDate.value = true;
                            startDateOptions.value[0].disabled = false;
                            startDateOptions.value[1].disabled = true;
                            computedStartDateOptions.value = startDateOptions.value;
                            return true;
                        }
                    }
                }
            }
            return true;
        } else {
            startDateOptions.value[0].disabled = false;
            startDateOptions.value[1].disabled = false;
            computedStartDateOptions.value = startDateOptions.value;
        }
    });
    startDate.value = new Date(startDateValue);
};
const updateEndDateModelValue = (endDateValue: string) => {
    endDateOptions.value[0].disabled = false;
    endDateOptions.value[1].disabled = false;
    computedEndDateOptions.value = endDateOptions.value;
    approvedDaysList.value.some((approvedDay, index) => {
        if (moment(approvedDay).isSame(endDateValue, 'day')) {
            if (approvedDaysList.value.length === 1) {
                if (moment(approvedDay).hour() === VACATION_TIME.MORNING) {
                    isMorningEndDate.value = false;
                    endDateOptions.value[0].disabled = true;
                    endDateOptions.value[1].disabled = false;
                } else if (moment(approvedDay).hour() === VACATION_TIME.EVENING) {
                    isMorningEndDate.value = true;
                    endDateOptions.value[0].disabled = false;
                    endDateOptions.value[1].disabled = true;
                }
            } else {
                const occuranceOfDays = approvedDaysList.value.reduce((acc, currentDay) => {
                    if (moment(currentDay).isSame(endDateValue, 'day')) acc++;
                    return acc;
                }, 0);
                if (occuranceOfDays === 2) {
                    endDateOptions.value[0].disabled = true;
                    endDateOptions.value[1].disabled = true;
                } else if (occuranceOfDays === 1) {
                    if (
                        moment(approvedDay)
                            .add(1, 'day')
                            .isSame(approvedDaysList.value[index + 1])
                    ) {
                        if (moment(approvedDay).hour() === VACATION_TIME.MORNING && moment(approvedDaysList.value[index - 1]).hour() === VACATION_TIME.NOON) {
                            // console.log('end DATE : MORNING');
                            isMorningEndDate.value = false;
                            endDateOptions.value[0].disabled = true;
                            endDateOptions.value[1].disabled = false;
                            computedEndDateOptions.value = endDateOptions.value;
                            return true;
                        } else if (moment(approvedDay).hour() === VACATION_TIME.EVENING && (moment(approvedDaysList.value[index + 1]).hour() === VACATION_TIME.EVENING || moment(approvedDaysList.value[index - 1]).hour() === VACATION_TIME.MORNING)) {
                            // console.log('end DATE : AFTERNOON');
                            isMorningEndDate.value = true;
                            endDateOptions.value[0].disabled = false;
                            endDateOptions.value[1].disabled = true;
                            computedEndDateOptions.value = endDateOptions.value;
                            return true;
                        } else if (moment(approvedDay).hour() === VACATION_TIME.MORNING && (moment(approvedDaysList.value[index + 1]).hour() === VACATION_TIME.EVENING || moment(approvedDaysList.value[index - 1]).hour() === VACATION_TIME.MORNING)) {
                            // console.log('end DATE : AFTERNOON');
                            isMorningEndDate.value = false;
                            endDateOptions.value[0].disabled = true;
                            endDateOptions.value[1].disabled = false;
                            computedEndDateOptions.value = endDateOptions.value;
                            return true;
                        }
                    } else {
                        if (moment(approvedDay).hour() === VACATION_TIME.MORNING) {
                            isMorningEndDate.value = false;
                            endDateOptions.value[0].disabled = true;
                            endDateOptions.value[1].disabled = false;
                            computedEndDateOptions.value = endDateOptions.value;
                            return true;
                        } else if (moment(approvedDay).hour() === VACATION_TIME.EVENING) {
                            isMorningEndDate.value = true;
                            endDateOptions.value[0].disabled = false;
                            endDateOptions.value[1].disabled = true;
                            computedEndDateOptions.value = endDateOptions.value;
                            return true;
                        }
                    }
                }
            }
        } else {
            endDateOptions.value[0].disabled = false;
            endDateOptions.value[1].disabled = false;
        }
    });
    endDate.value = new Date(endDateValue);
};
</script>
<template>
    <form @submit.prevent="onSubmit">
        <div class="card p-fluid big-screen">
            <h4>{{ t('vacationRequestFormTitle') }}</h4>
            <div class="field">
                <label for="leaveTypeLabel">{{ t('leaveType') }}</label>
                <Dropdown
                    data-testid="leave-request-leave-type-dropdown"
                    id="leaveTypeLabel"
                    v-model="selectedVacationType"
                    :class="{ 'p-invalid': !!vacationTypeError }"
                    :options="props.leaveTypeItems"
                    optionLabel="vacationTypeName"
                    :placeholder="$t('LeaveTypeDropdownPlaceholder')"
                    @update:model-value="handleVacationTypeChange"
                ></Dropdown>
                <small data-testid="leave-request-dropdown-error-msg" class="p-error" v-show="vacationTypeError">{{ $t('VacationTypeLeaveErrorMessage') }}</small>
            </div>
            <div class="field">
                <label for="from">{{ $t('FromLabel') }}</label>
                <div>
                    <Calendar
                        data-testid="leave-request-calendar-start-date"
                        dateFormat="dd/mm/yy"
                        id="from"
                        :disabled="isCalenderDisabled"
                        :class="{ 'p-invalid': !!startDateError, 'mb-3': !startDateError }"
                        class="mb-1"
                        :model-value="startDate"
                        @update:model-value="updateStartDateModelValue"
                        :disabled-days="[0, 6]"
                        :disabledDates="filteredDaysList"
                        :placeholder="t('StartingDateErrorMessage')"
                        showIcon
                        @month-change="handleMonthChange"
                        @show="handleGetInitialMonthDisabledDays"
                    />
                    <small data-testid="leave-request-calender-start-error-msg" class="p-error" v-show="startDateError">{{ $t('StartingDateErrorMessage') }}</small>

                    <SelectButton
                        data-testid="leave-request-select-btn-start-date"
                        v-model="isMorningStartDate"
                        :class="{ 'p-invalid': !!morningStartDateError, 'mt-1': startDateError }"
                        :options="computedStartDateOptions"
                        optionValue="value"
                        optionLabel="name"
                        option-disabled="disabled"
                    />
                    <small data-testid="leave-request-select-btn-start-date-error-msg" class="p-error" v-show="morningStartDateError">{{ $t('StartDatePeriod') }}</small>
                </div>
            </div>
            <div class="field">
                <label for="to">{{ $t('ToLabel') }}</label>
                <div>
                    <Calendar
                        data-testid="leave-request-calendar-end-date"
                        dateFormat="dd/mm/yy"
                        id="to"
                        :class="{ 'p-invalid': !!endDateError, 'mb-3': !endDateError }"
                        class="mb-1"
                        :disabled-days="[0, 6]"
                        :disabled="isCalenderDisabled"
                        :disabledDates="filteredDaysList"
                        :model-value="endDate"
                        @update:model-value="updateEndDateModelValue"
                        :placeholder="t('EndingDateErrorMessage')"
                        showIcon
                        @month-change="handleMonthChange"
                        @show="handleGetInitialMonthDisabledDays"
                    />
                    <small data-testid="leave-request-calendar-end-date-error-msg" class="p-error" v-if="endDateError">{{ $t(`${endDateError}`) }}</small>
                    <SelectButton
                        data-testid="leave-request-select-btn-end-date"
                        id="to"
                        v-model="isMorningEndDate"
                        :class="{ 'p-invalid': !!morningEndDateError, 'mt-2': endDateError }"
                        :options="endDateOptions"
                        optionValue="value"
                        optionLabel="name"
                        option-disabled="disabled"
                    />
                    <small data-testid="leave-request-end-date-select-btn-error-msg" :class="{ 'p-invalid': !!morningEndDateError, 'mt-2': !!morningEndDateError }" class="p-error" v-show="morningEndDateError">{{ $t('EndingDatePeriod') }}</small>
                </div>
            </div>
            <div class="field">
                <label for="justification">{{ $t('JustificationLabel') }}</label>
                <Textarea data-testid="leave-justification-textarea" id="justification" v-model="comment" :class="{ 'p-invalid': !!commentError }" rows="3" />
                <small data-testid="leave-request-justification-textarea-error-msg" class="p-error" v-if="commentError">{{ $t(commentError) }}</small>
            </div>
            <div class="field">
                <div class="flex flex-row justify-content-between pb-1 border-bottom-2 surface-border">
                    <span class="text-VACATION_TIME.MORNING00 text-md font-semibold mr-2 mb-1 md:mb-0">{{ $t('AllowanceText') }}</span>
                    <span class="text-VACATION_TIME.MORNING00 text-md font-semibold mr-2 mb-1 md:mb-0">{{ $t('ApprovedText') }}</span>
                </div>
                <div class="flex flex-row justify-content-between">
                    <span class="text-md font-medium text-600 mr-2">{{ currentQuota }}</span>
                    <span class="text-md font-medium text-600 mr-2" :class="{ 'text-red-600': isQuotaNegative, 'font-semiBold': isQuotaNegative }">{{ ifApprovedQuota }}</span>
                </div>
            </div>
            <div class="justify-content-center">
                <Button data-testid="leave-request-submit-btn" type="submit" :label="$t('LeaveRequestSubmitButton')" :disabled="isSubmitting || isQuotaNegative" class="mr-5 mb-5 mt-2"></Button>
            </div>
        </div>
    </form>
</template>

<style scoped>
@media only screen and (min-width: 850px) {
    .big-screen {
        max-width: 45rem;
    }
}
</style>
