import { expect } from '@storybook/jest';
import type { Meta, StoryObj } from '@storybook/vue3';
import AllLeavesTable from '@/components/AllLeavesTable.vue';
import { within } from '@storybook/testing-library';
import { sleep } from '@/utils/SleepFunction';
import type { VacationRequestType } from '@/types/vacationRequestType';

const meta: Meta<typeof AllLeavesTable> = {
    title: 'All Leaves Table',
    component: AllLeavesTable,
    render: (args: any) => ({
        components: { AllLeavesTable },
        setup() {
            return { args };
        },
        template: '<AllLeavesTable :vacationRequestList="args.vacationRequestList" />'
    }),
    tags: ['autodocs'],
    argTypes: {
        vacationRequestList: {
            name: 'vacationRequestData',
            control: 'object',
            type: { name: 'string', required: true },
            description: 'The vacation data to be populated in the array',
            table: {
                type: {
                    summary: 'array',
                    detail: '[{   userId: number,startDate: string,endDate: string,status: Status,requestDateCreation: string,morningStartDate: booleanmorningEndDate: boolean,nbOfDays: number,comment: string,username: string,entityId: number,range?: string;  }]'
                }
            }
        }
    }
};

export default meta;

type story = StoryObj<typeof AllLeavesTable>;

export const Default: story = {
    args: {
        vacationRequestList: [
            {
                vacationRequestId: 'ed88d66b-08d7-4949-8dde-b5fbdf06add2',
                startDate: '2023-02-10',
                endDate: '2023-02-12',
                createdAt: '2023-02-08',
                comment: 'some text',
                status: 'APPROVED',
                nbOfDays: 2,
                collaborator: {
                    role: 'Collaborator',
                    firstName: 'Marwen',
                    lastName: 'Ayoub',
                    collaboratorId: '3e722498-cb00-4979-90b0-a1f8b4d9131c'
                },
                manager: {
                    role: 'Manager',
                    firstName: 'Aymen',
                    lastName: 'HOSNI',
                    collaboratorId: 'c8d6f050-4d2a-44dc-8cab-86618a5645c7'
                },
                vacationType: {
                    vacationTypeId: '2666',
                    vacationTypeName: 'RTT',
                    yearlyQuota: 25
                }
            },
            {
                vacationRequestId: 'ed88d66b-08d7-4949-8dde-b5fbdf06add2',
                startDate: '2023-10-03',
                endDate: '2023-10-05',
                createdAt: '2023-02-08',
                nbOfDays: 2.5,
                comment: 'some text',
                status: 'PENDING',
                collaborator: {
                    role: 'Collaborator',
                    firstName: 'Marwen',
                    lastName: 'Ayoub',
                    collaboratorId: '3e722498-cb00-4979-90b0-a1f8b4d9131c'
                },
                manager: {
                    role: 'Manager',
                    firstName: 'Aymen',
                    lastName: 'HOSNI',
                    collaboratorId: 'c8d6f050-4d2a-44dc-8cab-86618a5645c7'
                },
                vacationType: {
                    vacationTypeId: '2666',
                    vacationTypeName: 'RTT',
                    yearlyQuota: 25
                }
            }
        ]
    },
    play: async ({ canvasElement, args }) => {
        const canvas = within(canvasElement);
        await expect(await canvas.findByText('My Vacation List')).toBeInTheDocument();
        sleep(2000);
        args.vacationRequestList.forEach(async (el: VacationRequestType) => {
            await sleep(2000);
            expect(canvas.getAllByText(el.status)[0]).toBeInTheDocument();
            await sleep(500);
            expect(canvas.getAllByText(el.vacationType.vacationTypeName.trim())[0]).toBeInTheDocument();
            await sleep(500);
            expect(canvas.getAllByText(`${el.manager?.firstName} ${el.manager?.lastName}`)[0]).toBeInTheDocument();
            await sleep(500);
            expect(canvas.getAllByText(`${el.nbOfDays} days`)[0]).toBeInTheDocument();
        });
    }
};
