import { createApp } from 'vue';
import router from '@/router';

import App from '@/App.vue';
import { i18n } from '@/config/i18n';

import 'primevue/resources/themes/tailwind-light/theme.css'; //theme
import 'primevue/resources/primevue.min.css'; //core css
import 'primeicons/primeicons.css';
import PrimeVue from 'primevue/config';
import DialogService from 'primevue/dialogservice';
import ConfirmationService from 'primevue/confirmationservice';
import ToastService from 'primevue/toastservice';
import Toast from 'primevue/toast';

import '@/assets/styles.scss';

const app = createApp(App);

app.component('Toast', Toast);
app.use(ToastService);
app.use(ConfirmationService);
app.use(DialogService);
app.use(router);
app.use(PrimeVue, { ripple: true });
app.use(i18n);
app.mount('#app');
