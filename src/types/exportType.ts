import type { ExportArrayType } from '@/constants/constant';

export type ExportType = (typeof ExportArrayType)[number];
