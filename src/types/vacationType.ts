import type { components } from '@/service/schema';

export type vacationType = components['schemas']['VacationTypeDTO'];

export type newVacationType = Omit<vacationType, 'vacationTypeId'>;
