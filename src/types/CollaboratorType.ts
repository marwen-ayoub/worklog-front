import type { components } from '@/service/schema';

export type CollaboratorType = components['schemas']['CollaboratorDTO'];
