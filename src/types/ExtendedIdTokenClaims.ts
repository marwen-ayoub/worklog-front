import type { IdTokenClaims } from 'oidc-client-ts';

export interface ExtendedIdTokenClaims extends IdTokenClaims {
    manager_id: string;
    manager_firstName: string;
    manager_lastName: string;
    address: {
        country: string;
        formatted: string;
        region: string;
    };
    realm_access: {
        roles: string[];
    };
}
