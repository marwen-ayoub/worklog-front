export type SideBarMenuItemsType = {
    label: string;
    items: MenuItem[];
    separator?: boolean;
};
export type MenuItem = {
    label: string;
    icon: string;
    to: string;
};
