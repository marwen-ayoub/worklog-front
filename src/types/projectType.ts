import type { components } from '@/service/schema';

export type ProjectType = components['schemas']['Project'];
