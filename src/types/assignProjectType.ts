import type { components } from '@/service/schema';

export type AssignProjectType = components['schemas']['AssignProjectDTO'];
