import type { LeaveStatusType, ImputationStatusList } from '@/constants/constant';
import type { vacationType } from './vacationType';
export type Status = (typeof LeaveStatusType)[number];
export type ImputationStatus = (typeof ImputationStatusList)[number];

export type chartDataType = {
    labels: string[];
    datasets: {
        label: string;
        backgroundColor: string;
        borderColor: string;
        data: number[];
    }[];
};

export type LeaveBalanceType = {
    vacationType: string;
    used: number;
    availavle: number;
    allowance: number;
};

export type dropdownLeaveType = {
    name: string;
    code: string;
};

export type NextLeaveType = {
    id: number;
    nbOfDays: number;
    startDate: string;
    endDate: string;
    vacationType: string;
    status: Status;
};

export type vacationDayType = {
    id: number;
    vacationTypeName: string;
    vacationYearQuota: number;
};
export interface MorningDateValue {
    name: string;
}

export type vacationFormType = {
    vacationType: vacationType;
    startDate: Date;
    endDate: Date;
    morningStartDate: NonNullable<boolean | undefined>;
    morningEndDate: NonNullable<boolean | undefined>;
    comment?: string | undefined;
};

export type vacationRequestDtoType = {
    userId: number;
    startDate: string;
    endDate: string;
    status: Status;
    requestDateCreation: string;
    morningStartDate: boolean;
    morningEndDate: boolean;
    nbOfDays: number;
    comment: string;
    username: string;
    entityId: number;
};
export type vacationRequestType = vacationRequestDtoType & { range: string };
