import type { TimesheetInputRowType, Role, ImputationMonthlyStatusList, SaveImputationType, MANAGER_RESPONSE } from '@/constants/constant';
import type { components } from '@/service/schema';

type DailyTimesheet = components['schemas']['TimesheetEntryDTO'];

export type TimesheetData = components['schemas']['WeeklyTimesheetDTO'];
export type ProjectEntry = {
    assignationId: number;
    timesheetEntries: DailyTimesheet[];
};

type project = components['schemas']['Project'];

export type TimesheetEntry = {
    assignationId: number | undefined;
    project: project;
    startDate?: string;
    endDate?: string | null;
    mondayEntry: {
        date: string | undefined;
        value: number | undefined;
        entryDisabled: boolean | undefined;
    };
    tuesdayEntry: {
        date: string | undefined;
        value: number | undefined;
        entryDisabled: boolean | undefined;
    };
    wednesdayEntry: {
        date: string | undefined;
        value: number | undefined;
        entryDisabled: boolean | undefined;
    };
    thursdayEntry: {
        date: string | undefined;
        value: number | undefined;
        entryDisabled: boolean | undefined;
    };
    fridayEntry: {
        date: string | undefined;
        value: number | undefined;
        entryDisabled: boolean | undefined;
    };
    projectTotalHours: number;
    offProject: boolean;
};

export type DaysErrorsMessage = {
    mondayEntryError: boolean | undefined;
    tuesdayEntryError: boolean | undefined;
    wednesdayEntryError: boolean | undefined;
    thursdayEntryError: boolean | undefined;
    fridayEntryError: boolean | undefined;
};

export type TimesheetMonthlyStatus = {
    id: number;
    timesheetMonth: string;
    collaborator: {
        id: number;
        collaboratorFirstName: string;
    };
    monthStatus: MonthlyStatus;
};
export type FormattedTimesheetMonthlyStatus = {
    monthlyTimesheetId?: number;
    timesheetMonth: number;
    timesheetYear: number;
    monthStatus: MonthlyStatus;
    createdAt?: Date;
};

export type ImputationTimesheetDraft = WeeklyDraftEntries;
export type LastWeekOfMonthEntries = WeeklyDraftEntries;
export type WeeklyDraftEntries = {
    assignationId: number;
    timesheetEntries: DailyTimesheet[];
};
export type OffProjectType = {
    assignationId: number;
    project: project;
    startDate: string;
    endDate: string | null;
};
export type PublicHolidayType = components['schemas']['PublicDays'];

export type ImputationRequestDataType = components['schemas']['ImputationRequest'];

export type DisplayedMonthlyTimesheetRequestsType = {
    currentPage: number;
    totalPages: number;
    lastPage: boolean;
    totalItems: number;
    data: ImputationRequestDataType[];
};

export type MonthlyTimesheetValidationType = components['schemas']['MonthlyTimesheetResponseDTO'];

export type ManagerResponseType = MANAGER_RESPONSE.APPROVED | MANAGER_RESPONSE.REJECTED;
export type MonthlyTimesheetStatusType = 'APPROVED' | 'REJECTED' | 'PENDING';

export type TimesheetInputType = (typeof TimesheetInputRowType)[number];
export type WeeklySaveType = (typeof SaveImputationType)[number];
export type RoleType = (typeof Role)[number];
export type MonthlyStatus = (typeof ImputationMonthlyStatusList)[number];
