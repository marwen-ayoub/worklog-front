import type { components } from '@/service/schema';

export type VacationDaysQuotaType = components['schemas']['VacationDaysQuotaDTO'];

export type ExtendedVacationDaysQuotaType = components['schemas']['VacationDaysQuota'];
