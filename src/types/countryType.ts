import type { components } from '@/service/schema';

export type CountryType = components['schemas']['Country'];

export type PublicDayType = components['schemas']['PublicDays'];

export type newPublicDayType = Omit<PublicDayType, 'id'>;
