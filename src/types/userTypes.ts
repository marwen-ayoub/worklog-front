import type { RoleType } from '@/types/imputationTypes';
import type { components } from '@/service/schema';

export type UserType = {
    collaboratorId: string;
    firstName: string;
    lastName: string;
    role: RoleType | undefined;
    address?: AddressType;
    manager?: { collaboratorId: string; firstName?: string; lastName?: string; role?: RoleType };
};

export type AddressType = components['schemas']['Address'];
export type UserRoleType = 'COLLABORATOR' | 'MANAGER' | undefined;

export type userClaimsType = {
    given_name: string;
    family_name: string;
    sub: string;
    manager_id: string;
    address: {
        formatted: string;
        region: string;
        country: string;
    };
    [key: string]: any;
};
