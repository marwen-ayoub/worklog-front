import type { LeaveStatusType } from '@/constants/constant';
import type { components } from '@/service/schema';

export type Status = (typeof LeaveStatusType)[number];

export type VacationRequestType = components['schemas']['VacationRequest'];

export type NewVacationRequest = Omit<VacationRequestType, 'nbOfDays' | 'status'>;

export type VacationRequestPageType = components['schemas']['VacationRequestPageDTO'];

export type VacationRequestDatesList = components['schemas']['VacationRequestDatesDTO'];
