import type { Preview } from '@storybook/vue3';
import { setup } from '@storybook/vue3';
import PrimeVue from 'primevue/config';
import { i18n } from '../src/config/i18n';
import ToastService from 'primevue/toastservice';
import ConfirmationService from 'primevue/confirmationservice';
import '@/assets/styles.scss';
// theme
import 'primevue/resources/themes/lara-light-blue/theme.css';
// core
import 'primevue/resources/primevue.min.css';
// icons
import 'primeicons/primeicons.css';

setup((app) => {
    app.use(PrimeVue, { ripple: true });
    app.use(i18n);
    app.use(ToastService);
    app.use(ConfirmationService);
});

const preview: Preview = {
    parameters: {
        actions: { argTypesRegex: '^[a-z].*' },
        controls: {
            matchers: {
                color: /(background|color)$/i,
                date: /Date$/
            }
        }
    }
};

export default preview;
